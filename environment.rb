require "bundler/setup"
require "digest/sha1"

Bundler.require

STATUSES = ["Ignore", "Low Priority", "High Priority", "Critical"]

require File.expand_path(File.dirname(__FILE__) + "/models.rb")
require File.expand_path(File.dirname(__FILE__) + "/app.rb")
