$(document).ready(function(){
    $(".blob").each(function(key, val){
	var sha = val.id.split('_')[1]
	$.get('/blob/' + sha, function(data){
	    $(val).html(data)
	})
    });

    $("select.classify").change(function(){
	var sha = this.id.split("classify_")[1]
	var classification = $(this).val()
	var el = this;
	$.get("/classify/" + sha + "/" + classification, function(){
	    $(el).parent().parent().hide()
	    var count = parseInt($("#count").html())
	    $("#count").html(count - 1)
	})
    })

})
